using Test

function testMtxPot()
    for i in 1 : 1000000
        size = rand(1:6)
        P = rand(1:10)
        M = rand(-12:12,size,size)
        @test matrix_pot(M,P) == M^P
    end
    println("OK")
end

function testMtxPotSqr()
    for i in 1 : 1000000
        size = rand(1:6)
        P = rand(1:10)
        M = rand(-12:12,size,size)
        @test matrix_pot_by_squaring(M,P) == M^P
    end
    println("OK")
end

function multiplica(a, b)

    dima = size(a)
    dimb = size(b)

    if dima[2] != dimb[1]
        return -1
    end

    c = zeros(dima[1], dimb[2])

    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end

    return c
end

function matrix_pot(M, p)

    N = M

    for i in 2 : p
        M = multiplica(M,N)
    end

    return M

end

testMtxPot()

function matrix_pot_by_squaring(M, p)

    K = 1
    if p == 1
        K *= M
    elseif p % 2 == 0
        K *= matrix_pot_by_squaring(multiplica(M,M), p/2)
    elseif p % 2 != 0
        K *= M * matrix_pot_by_squaring(multiplica(M,M), (p-1)/2)
    end

    return K

end

testMtxPotSqr()

function compare_times()

    size = 100
    p = 50
    M = rand( size, size )

    @time matrix_pot(M, p)
    @time matrix_pot_by_squaring(M, p)
end

compare_times()
